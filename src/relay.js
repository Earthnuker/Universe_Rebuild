const pubsub = require('pubsub-swarm');
const ssbkeys = require("ssb-keys");
const swarm_defaults = require('dat-swarm-defaults');
const low = require('lowdb');
const _id = require('lodash-id');
const FileSync = require('lowdb/adapters/FileSync');
const Memory = require('lowdb/adapters/Memory');
const events = require('events');
const yaml = require('js-yaml');
const nat = require('natman-api');
const ev_channel = new events.EventEmitter();
const argv = require('yargs').parse(process.argv);
var conf = require('rc')("Universe_relay", {
    port: 0,
    instance: "Universe",
    forward: false,
}, argv);

const universe_keys = ssbkeys.loadOrCreateSync("relay.keys");

var sw = pubsub(conf.instance,
    swarm_defaults(
        {
            utp: true, tcp: false,
            port: conf.port
        }
    )
);

sw.id = sw.gossip.keys.id;

const db = low(
    new FileSync('relay_db.yaml',
        {
            serialize: (data) => {
                data.timestamp = new Date();
                delete data.signature;
                var s_data = ssbkeys.signObj(universe_keys, data);
                return yaml.dump(s_data);
            },
            deserialize: (data) => {
                data = yaml.load(data);
                if (data.signature) {
                    if (!ssbkeys.verifyObj(universe_keys, data)) {
                        console.error("DB Signature invalid, nuking DB");
                        data = {};
                    };
                } else {
                    console.warn("no signature found");
                };
                return data;
            }
        }
    )
);
db._.mixin(_id);
db._.createId = (collectionName, item) => (crypto.randomBytes(4).toString("hex"));
db.defaults({ vessels: [] }).write();


function push_send(data, type, to = undefined) {
    var chunk = { from: sw.id, to: to, type: type, data: data };
    console.log("SEND:", chunk);
    sw.publish(chunk);
}

function db_push(target) {
    var sb_snap = { vessels: {} };
    db.get("vessels").value().forEach((vessel) => {
        sb_snap.vessels[vessel.id] = new Date(vessel.t).toJSON();
    });
    console.log("pushing db to", target);
    push_send(sb_snap, 'state', target);
}

function handle_data(data) {
    console.log("RECV:", data);
    ev_channel.emit(data.type, data.data, data.from);
}

ev_channel.on('state', (data, from) => {
    var to_update = [];
    for (var vess_id in data.vessels) {
        var vess_t = new Date(data.vessels[vess_id]);
        var my_vessel = db.get("vessels").getById(vess_id).value();
        if (my_vessel == undefined) {
            to_update.push(vess_id);
        } else {
            var my_t = new Date(my_vessel.t)
            if (my_t < vess_t) {
                to_update.push(vess_id);
            }
        }
    }
    if (to_update.length > 0) {
        push_send(to_update, "get", from);
    };
});

ev_channel.on('get', (data, from) => {
    if (data.length != 0) {
        var ret = [];
        data.forEach((vess_id) => {
            ret.push(db.get("vessels").getById(vess_id).value());
        })
        push_send(ret, "update", from);
    };
});

ev_channel.on('update', (data, from) => {
    data.forEach((vessel) => {
        db.get("vessels").upsert(vessel).write();
    });
});



sw.on('message', handle_data);
sw.swarm.on("connecting", (peer) => {
    console.log("Connecting to " + peer.id);
})
sw.swarm.on("connection", (conn) => {
    push_send({}, "hello");
    db_push();
})
var port = sw.swarm.address().port;
console.log("Relay ready on port " + port + " with ID " + sw.id);
console.log("Trying to forward port...");


if (conf.forward) {
    try {
        nat(port, port);
    } catch (err) {
        console.warn("Automatic port-forwarding failed: " + err.msg);
    }
}