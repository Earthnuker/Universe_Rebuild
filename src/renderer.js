'use strict';
const electron = require('electron');
const remote = electron.remote;
const $ = require('jquery');
const stream = require('stream');
const crypto = require('crypto');
const asciify = require("asciify");
const pubsub = require('pubsub-swarm');
const ssbkeys = require("ssb-keys");
const swarm_defaults = require('dat-swarm-defaults');
const yaml = require('js-yaml');
const js_vm = require('vm');
const fs = require('fs');
const rc = require('rc');
const d3 = require('d3');
const ForceGraph3D = require('3d-force-graph');
const ForceGraph = require('force-graph');
const nat = require('natman-api');
const handlebars = require('handlebars');
const _ = require('lodash');
const low = require('lowdb');
const _id = require('lodash-id');
const FileSync = require('lowdb/adapters/FileSync');
const Memory = require('lowdb/adapters/Memory');
const events = require('events');
const jkstra = require('jkstra');
const jsonata = require("jsonata");
const ev_channel = new events.EventEmitter();
const argv = require('yargs').parse(remote.process.argv);


var conf = require('rc')("Universe", {
    port: 0,
    instance: "Universe",
    test: false
}, argv);

if (process.env.PORTABLE_EXECUTABLE_DIR) {
    conf.test = true;
}

console.log(conf);

const universe_keys = ssbkeys.loadOrCreateSync("universe.keys");

function show_3d_map() {
    var graph = universe.js_graph();
    var elem = document.getElementById('map');
    const Graph = ForceGraph3D()(elem)
        .forceEngine("ngarph")
        .graphData(graph)
        .nodeLabel('name')
        .nodeAutoColorBy('stem')
        .nodeVal(node => {
            if (node.paradox) {
                return 2.0;
            } else {
                return 0.5;
            }
        })
        .linkWidth(0.25)
        .backgroundColor("#000000")
        .linkColor((link) => {
            return 'rgba(255,255,255,0.5)';
        })
        .linkDirectionalParticleColor((link) => {
            return 'rgba(255,255,255,0.5)';
        })
        .onNodeClick(node => {
            const distance = 40;
            const distRatio = 1 + distance / Math.hypot(node.x, node.y, node.z);
            Graph.cameraPosition(
                { x: node.x * distRatio, y: node.y * distRatio, z: node.z * distRatio }, // new position
                node, // lookAt ({ x, y, z })
                3000  // ms transition duration
            )
        })
        .onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
        .cooldownTime(Infinity)
        .linkDirectionalParticles(5)
        .linkDirectionalParticleSpeed(0.002);
    return Graph;
}


function show_map() {
    var graph = universe.js_graph();
    var elem = document.getElementById('map');
    var width = elem.getBoundingClientRect().width;
    var height = elem.getBoundingClientRect().height;
    var clicked = [];
    var route_src = [];
    var route_dst = [];
    const Graph = ForceGraph()(elem)
        .graphData(graph)
        .nodeLabel('name')
        .nodeAutoColorBy('stem')
        .nodeVal(node => {
            if (node.paradox) {
                return 2.0;
            } else {
                return 0.5;
            }
        })
        .cooldownTime(Infinity)
        .d3VelocityDecay(0.25)
        .d3Force("link", d3.forceLink().distance(50).iterations(100))
        .d3Force("charge", d3.forceManyBody().strength(-400))
        .d3Force("center", d3.forceCenter())
        .d3Force("x", d3.forceX())
        .d3Force("y", d3.forceY())
        .linkWidth(0.5)
        .backgroundColor("#000000")
        .linkColor((link) => {
            if (route_src.indexOf(link.source.id) != -1) {
                if (route_dst.indexOf(link.target.id) != -1) {
                    return 'rgba(255,0,0,0.5)';
                }
            }
            if (route_dst.indexOf(link.source.id) != -1) {
                if (route_src.indexOf(link.target.id) != -1) {
                    return 'rgba(255,0,0,0.5)';
                }
            }
            return 'rgba(255,255,255,0.5)';
        })
        .linkDirectionalParticleColor((link) => {
            if (route_src.indexOf(link.source.id) != -1) {
                if (route_dst.indexOf(link.target.id) != -1) {
                    return 'rgba(255,0,0,0.5)';
                }
            }
            if (route_dst.indexOf(link.source.id) != -1) {
                if (route_src.indexOf(link.target.id) != -1) {
                    return 'rgba(255,0,0,0.5)';
                }
            }
            return 'rgba(255,255,255,0.5)';
        })
        .onNodeClick(node => {
            clicked.push(node.id);
            clicked = clicked.slice(-2);
            if (clicked.length == 2) {
                route_src = [];
                route_dst = [];
                var path = universe.id_route(clicked[0], clicked[1]) || [];
                term.set_command("route \"" + clicked[0] + "\" \"" + clicked[1] + "\"")
                path.forEach((edge) => {
                    route_src.push(edge.data.src);
                    route_dst.push(edge.data.dst);
                });
            }
        })
        .onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
        .linkDirectionalParticles(3)
        .linkDirectionalParticleSpeed(0.01);
    return Graph;
}

function get_stem(node_id) {
    var path = [];
    var node;
    while (true) {
        node = db.get("vessels").getById(node_id).value()
        if (node == undefined) {
            return undefined;
        };
        path.push(node.id);
        if (node_id == node.parent) {
            return node_id;
        };
        if (path.indexOf(node.parent) != -1) {
            return path.sort()[0];
        }
        node_id = node.parent;
    };
}

function remove_articles(str) {
    var s = ` ${str} `;
    s = s.replace(/ a /g, ' ')
    s = s.replace(/ an /g, ' ')
    s = s.replace(/ the /g, ' ')
    s = s.replace(/ of /g, ' ')
    s = s.replace(/ some /g, ' ')
    s = s.replace(/ one /g, ' ')
    s = s.replace(/ two /g, ' ')
    return s.trim()
}

var vessel_location = null;
var vessel = null;
var adapter;
if (conf.test == true) {
    adapter = new Memory();
} else {
    adapter = new FileSync('db.yaml',
        {
            serialize: (data) => {
                data.timestamp = new Date();
                delete data.signature;
                console.log(data);
                var s_data = ssbkeys.signObj(universe_keys, data);
                return yaml.dump(s_data);
            },
            deserialize: (data) => {
                data = yaml.load(data);
                if (data.signature) {
                    if (!ssbkeys.verifyObj(universe_keys, data)) {
                        console.error("DB Signature invalid, nuking DB");
                        data = {};
                    };
                } else {
                    //data = {};
                    console.warn("no signature found");
                };
                return data;
            }
        });
}

const db = low(adapter);
db._.mixin(_id);
db._.createId = (collectionName, item) => (crypto.randomBytes(4).toString("hex"));
db.defaults({ vessels: [] }).write();

var universe = {
    "show_map": (type) => {
        var te = $('#terminal')[0];
        te.visible = false;
        te.hidden = true;
        var me = $('#map')[0];
        me.style.display = "block";
        if (type == "3d") {
            window.graph = show_3d_map();
        } else if (type == "2d") {
            window.graph = show_map();
        } else {
            console.error("Invalid map type: " + type);
            return;
        }
        d3.select("body")
            .on("keydown", () => {
                if (d3.event.code == "F5") {
                    //update_map();
                };
                if (d3.event.code == "Escape") {
                    scroll_to_bottom();
                    var te = $('#terminal')[0];
                    te.visible = true;
                    te.hidden = false;
                    var me = $('#map')[0];
                    me.style.display = "none";
                    window.graph.stopAnimation();
                    window.graph = undefined;
                    d3.selectAll("#map > div").remove();
                };
            });
    },
    "js_graph": () => {
        var graph = {
            nodes: [],
            links: []
        };
        db.get("vessels").value().forEach((vessel) => {
            graph.nodes.push({
                id: vessel.id,
                name: get_vessel_name(vessel.id, false, true),
                stem: get_stem(vessel.id),
                paradox: vessel.parent == vessel.id,
                vessel: vessel
            });
            graph.links.push({
                source: vessel.id,
                target: vessel.parent
            });
        });
        return graph;
    },
    "children": (vessel_id) => {
        return db.get("vessels").filter({ parent: vessel_id }).value();
    },
    "find_children": (vessel_id, name) => {
        name = remove_articles(name);
        var ret = [];
        universe.children(vessel_id).forEach((vessel) => {
            var vessel_name = remove_articles((vessel.attr.join(" ") + " " + vessel.name));
            if (vessel_name.indexOf(name) != -1) {
                if (vessel.flags.hidden == false) {
                    ret.push(vessel);
                }
            }
        });
        return ret;
    },
    "add": (v_name, opts) => {
        if (!opts) opts = {};
        if (!opts.note) opts.note = null;
        if (opts.parent == undefined) {
            opts.parent = vessel_location;
        };
        var data = remove_articles(v_name.trim()).trim().split(/\s+/).filter(n => n);
        var new_vessel = {
            attr: data.slice(0, -1),
            name: data.slice(-1)[0],
            parent: opts.parent,
            owner: vessel,
            flags: {
                silent: false,
                hidden: false,
                locked: false,
                tunnel: false,
            },
            t: new Date(),
            note: opts.note,
            forum: [],
            programs: {},
        };
        if (opts.id != undefined) {
            new_vessel.id = opts.id;
        }
        var ret = db.get("vessels").upsert(new_vessel).write();
        return ret.id;
    },
    "search": (regex, to_search) => {
        var regex = new RegExp(regex);
        if (to_search == undefined) {
            to_search = db.get("vessels").value();
        }
        return to_search.filter((v) => {
            var j_name = v.attr.join(" ").trim() + " " + v.name.trim();
            return regex.exec(j_name.trim());
        })
    },
    "locate": (name) => {
        var db_ret = db.get("vessels").getById(name).value();
        if (db_ret != undefined) {
            return [db_ret];
        }
        name = remove_articles(name);
        return universe.search(name);
    },
    "visible": (location) => {
        var ret = [];
        db.get("vessels").filter({ parent: location, flags: { hidden: false } }).value().forEach((vessel) => {
            if (vessel.id != location) {
                ret.push(vessel);
            }
        });
        return ret;
    },
    "find_visible": (location, name) => {
        if (name == undefined) {
            return universe.visible(location);
        }
        name = remove_articles(name);
        return universe.search(name, universe.visible(location));
    },
    "route": (src, dst) => {
        var ret = [];
        try {
            var src_v = universe.locate(src)[0].id;
            var dst_v = universe.locate(dst)[0].id;
        } catch (error) {
            console.error(error);
            return null;
        }
        return universe.id_route(src_v, dst_v);
    },
    "id_route": (src_id, dst_id) => {
        var graph = new jkstra.Graph();
        var vessels = db.get("vessels").filter().value();
        var nodes = {}
        vessels.forEach((vessel) => {
            nodes[vessel.id] = graph.addVertex(vessel);
        });
        vessels.forEach((vessel) => {
            graph.addEdge(nodes[vessel.id], nodes[vessel.parent], { src: vessel.id, dst: vessel.parent });

            if (vessel.flags.hidden == false) {
                graph.addEdge(nodes[vessel.parent], nodes[vessel.id], { src: vessel.parent, dst: vessel.id });
            };
        });
        var dijkstra = new jkstra.algos.Dijkstra(graph);
        return dijkstra.shortestPath(nodes[src_id], nodes[dst_id]);
    }
};


universe.add("central nexus", { id: '0', note: "the center of everything", parent: '0' });
while (true) {
    var vess = db.get("vessels").value()[Math.floor(Math.random() * db.get("vessels").value().length)];
    if ((vess.flags.hidden == false) && (vess.flags.locked == false)) {
        vessel_location = vess.id;
        break;
    }
}

function escapeHtml(html) {
    var text = document.createTextNode(html);
    var div = document.createElement('div');
    div.appendChild(text);
    return div.innerHTML;
}

function run_js(code) {
    var sb_db = low(new Memory());
    sb_db._.mixin(_id);
    sb_db._.createId = (collectionName, item) => (crypto.randomBytes(4).toString("hex"));
    sb_db.defaults({ vessels: [] }).write();
    sb_db.setState(db.getState());
    var ctx = {
        U: sb_db,
        _: _,
        print: term.echo,
        yaml: yaml,
    };
    var sandbox = js_vm.createContext(ctx);
    return js_vm.runInNewContext(code, sandbox);
}

function eval_template(template) {
    var loc = db.get("vessels").getById(vessel_location).value();
    var vess = undefined;
    if (vessel) {
        vess = db.get("vessels").getById(vessel).value();
        loc = db.get("vessels").getById(vess.parent).value();
    }
    var objs = Object.values(db.getState().vessels);
    var random_vessel = objs[Math.floor(Math.random() * objs.length)];
    var data = {
        U: db.getState().vessels,
        V: vess,
        L: loc,
        R: random_vessel,
    }
    console.log(data);
    return handlebars.compile(template)(data);
}

function get_vessel_name(vessel, html = true, with_id = true) {
    vessel = db.get("vessels").getById(vessel).value();
    var name = (vessel.attr.join(" ") + " " + vessel.name).trim();
    if (with_id) {
        name = name + " (" + vessel.id + ")";
    }
    if (html == true) {
        return "<vessel id=\"" + vessel.id + "\">" + escapeHtml(name) + "</vessel>";
    } else {
        return name;
    }
}

require('jquery.terminal')($);


var cmds = {
    "eval": [
        (cmd) => {
            var ret = run_js(cmd.rest);
            term.echo("Returned: " + ret);
            return true;
        },
        "Evaluate javascript code (use single quotes instead of double quotes)",
    ],
    "status": [
        (cmd) => {
            term.echo("Connected to " + sw.swarm.connections.length + " peers");
            term.echo("Universe contains " + db.get("vessels").value().length + " vessels total");
            return false;
        },
        "show status"
    ],
    "min": [
        () => {
            var window = remote.BrowserWindow.getFocusedWindow();
            window.minimize();
            return true;
        },
        "Minimize the window"
    ],
    "echo": [
        (cmd) => {
            term.echo(cmd.rest, { raw: true });
            return true;
        },
        "prints text to the terminal",
    ],
    "exit": [
        () => {
            remote.getCurrentWindow().close();
            return true;
        },
        "Exits",
    ],
    "map3d": [
        () => {
            universe.show_map("3d");
            return true;
        },
        "Shows a 3D-map of every known Vessel"
    ],
    "map": [
        () => {
            universe.show_map("2d");
            return true;
        },
        "Shows a map of every known Vessel"
    ],
    "clear_db": [
        (cmd) => {
            if (cmd.rest === "yes_im_sure") {
                db.get("vessels").removeWhere({}).write();
                vessel = undefined;
                vessel_location = universe.add("central nexus", { id: '0', note: "the center of everything", parent: '0' });
                term.echo("Database cleared");
                return true;
            } else {
                term.echo("[[bg;#ff0000;]/!\\ This will clear your local database /!\\ ]");
                term.echo("If you are sure run: clear_db yes_im_sure");
                return true;
            }
        },
        "clear the database"
    ],
    "help": [
        (cmd) => {
            cmd = cmd.rest;
            if (cmd) {
                try {
                    var help_text = cmds[cmd].slice(1).join("<br>").trim();
                } catch (err) {
                    var help_text = undefined;
                }
                if (help_text) {
                    term.echo(help_text, { raw: true });
                } else {
                    term.error("No help for command \"" + cmd + "\"");
                }

            } else {
                term.echo("Available commands:");
                console.log(cmds);
                Object.keys(cmds).sort().forEach((cmd) => {
                    var help_text = cmds[cmd][1];
                    if (help_text) {
                        term.echo("- " + cmd + ": " + help_text, { raw: true })
                    } else {
                        term.echo("- " + cmd)
                    }
                });
            }
            return true;
        },
        "Displays help for commands",
    ],
    "query": [
        (cmd) => {
            var res = jsonata(cmd.rest).evaluate(db.getState());
            if (res != undefined) {
                term.echo(yaml.dump(res));
            } else {
                term.echo(res);
            };
            return true;
        },
        'query the database using JSONata'
    ],
    "route": [
        (cmd) => {
            if (cmd.args[0] == undefined) {
                term.echo("Usage: route [source] [destination]");
                term.echo("or: route [destination]");
                return;
            }
            if (cmd.args[1] == undefined) {
                cmd.args.push(cmd.args[0]);
                cmd.args[0] = vessel_id;
            };
            var vessel_id = vessel_location;
            if (vessel != undefined) {
                var vessel_id = db.get("vessels").getById(vessel).value().parent;
            }
            var path = universe.route(cmd.args[0], cmd.args[1]);
            if (path == null) {
                term.error("Failed to compute route from " + cmd.args[0] + " to " + cmd.args[1])
                return true;
            }
            if (path.length == 0) {
                term.echo("Really?");
                return true;
            };
            var start = path[0].from.data;
            var end = path[path.length - 1].to.data;
            var start_name = get_vessel_name(start.id, false);
            var end_name = get_vessel_name(end.id, false);
            term.echo("Route from " + start_name + " to " + end_name);

            path.forEach(edge => {
                var src = edge.from.data;
                var dst = edge.to.data;
                var src_name = (src.attr.join(" ") + " " + src.name).trim();
                var dst_name = (dst.attr.join(" ") + " " + dst.name).trim();
                if (src.parent == dst.id) {
                    term.echo("- <cmd>leave</cmd>", { raw: true });
                } else {
                    term.echo("- <cmd>enter " + dst_name + "</cmd>", { raw: true });
                }
            });
            return true;
        },
        "Calculate route from one vessel to another",
        "Usage: route [source] [destination]",
        "or: route [destination]"
    ],
    "search": [
        (cmd) => {
            var found = universe.locate(cmd.rest);
            if (found.length > 0) {
                term.echo("Found " + found.length + " matching vessels:");
                found.forEach((v) => {
                    term.echo(" - " + get_vessel_name(v.id, true), { raw: true });
                });
            } else {
                term.echo("No matching vessels found");
            }
            return true;
        },
        "Search for vessels (search string is parsed as RegEx)",
    ]
}


var vessel_cmds = {
    "create": [
        (cmd) => {
            var vessel_id = vessel_location;
            if (vessel != undefined) {
                vessel_id = db.get("vessels").getById(vessel).value().parent;
            }
            var new_vessel = universe.add(cmd.rest, { parent: vessel_id });
            term.echo("created " + get_vessel_name(new_vessel, true), { raw: true });
        },
        "Create a new vessel",
    ],
    "enter": [
        (cmd) => {
            var vessel_id = vessel_location;
            if (vessel) {
                var vessel_id = db.get("vessels").getById(vessel).value().parent;
            }
            var locations = universe.find_visible(vessel_id, cmd.rest);
            if (locations.length == 0) {
                term.echo("no target vessel found");
                return;
            }
            if (locations.length > 1) {
                term.echo("more than one target vessel found:");
                locations.forEach((vessel) => {
                    term.echo("- " + get_vessel_name(vessel.id), { raw: true });
                })
                return;
            }
            if (vessel) {
                db.get("vessels").updateById(vessel, {
                    parent: locations[0].id,
                    t: new Date(),
                }).write()
            } else {
                vessel_location = locations[0].id;
            }
        },
        "enter a visible vessel"
    ],
    "leave": [
        () => {
            if (vessel != undefined) {
                var parent_loc = db.get("vessels").getById(vessel).value().parent;
                parent_loc = db.get("vessels").getById(parent_loc).value().parent;
                db.get("vessels").updateById(vessel, {
                    parent: parent_loc,
                    t: new Date(),
                }).write()
            } else {
                vessel_location = db.get("vessels").getById(vessel_location).value().parent;
            }
        },
        "leave current location and move into parent vessel"
    ],
    "become": [
        (cmd) => {
            var vessel_id = vessel_location;
            if (vessel) {
                vessel_id = db.get("vessels").getById(vessel).value().parent;
            }
            var locations = universe.find_visible(vessel_id, cmd.rest);
            if (locations.length == 0) {
                term.echo("no target vessel found");
                return;
            }
            if (locations.length > 1) {
                term.echo("more than one target vessel found:");
                locations.forEach((vessel) => {
                    term.echo("- " + get_vessel_name(vessel.id));
                })
                return;
            }
            vessel = locations[0].id;
        },
        "become a different vessel",
    ],
    "take": [
        (cmd) => {
            if (!vessel) {
                term.error("You need a vessel to take something");
                return;
            }
            var vessel_id = db.get("vessels").getById(vessel).value().parent;
            var vessels = universe.find_visible(vessel_id, cmd.rest);
            if (vessels.length == 0) {
                term.error("no target vessel found");
                return;
            }
            if (vessels.length > 1) {
                term.error("more than one target vessel found:");
                vessels.forEach((vessel) => {
                    term.error("- " + get_vessel_name(vessel.id));
                })
                return false;
            };
            db.get("vessels").updateById(vessels[0].id, {
                parent: vessel,
                t: new Date(),
            }).write()
            term.echo("took the " + get_vessel_name(vessels[0].id, false));
            return false;
        },
        "take something"
    ],
    "drop": [
        (cmd) => {
            if (!vessel) {
                term.error("You need a vessel to drop something");
                return;
            }
            var vessel_id = db.get("vessels").getById(vessel).value().parent;
            var vessels = universe.find_children(vessel, cmd.rest);
            if (vessels.length == 0) {
                term.error("no target vessel found");
                return;
            }
            if (vessels.length > 1) {
                term.error("more than one target vessel found:");
                vessels.forEach((vessel) => {
                    term.error("- " + get_vessel_name(vessel.id));
                })
                return false;
            };
            db.get("vessels").updateById(vessels[0].id, {
                parent: vessel_id,
                t: new Date(),
            }).write()
        },
        "drop seomething"
    ],
    "warp": [
        (cmd) => {
            var target = cmd.rest.trim().replace(/^to /, "").trim();
            var locations = universe.locate(target);
            if (locations.length == 0) {
                term.error("Target not found!");
                return false;
            }
            if (locations.length > 1) {
                term.error("more than one target vessel found:");
                locations.forEach((vessel) => {
                    term.error("- " + get_vessel_name(vessel.id));
                })
                return false;
            };
            if (vessel) {
                db.get("vessels").updateById(vessel, {
                    parent: locations[0].id,
                    t: new Date(),
                }).write()
            } else {
                vessel_location = locations[0].id;
            }
        },
        "warp to a specific vessel"
    ],
    "unbind": [
        () => {
            vessel = undefined;
        },
        "leave current vessel and become a ghost again"
    ],
    "fold": [
        (cmd) => {
            if (vessel) {
                db.get("vessels").updateById(vessel, {
                    parent: vessel,
                    t: new Date(),
                }).write()
            } else {
                term.error("you need a vessel to fold");
            }
        },
        "fold vessel into itself, creating a paradox"
    ],
    "note": [
        (cmd) => {
            var vessel_id = vessel_location;
            if (vessel) {
                vessel_id = db.get("vessels").getById(vessel).value().parent;
            };
            db.get("vessels").updateById(vessel_id, { t: new Date(), note: cmd.rest }).write();
        },
        "add description to vessel"
    ],
    "program": [
        (cmd) => {
            var action = cmd.args[0];
            var result = cmd.args.slice(1).join(" ");
            var vessel_id = vessel_location;
            if (vessel) {
                vessel_id = db.get("vessels").getById(vessel).value().parent;
            };
            var pgm = db.get("vessels").getById(vessel_id).value().programs || {};
            pgm[action] = result;
            db.get("vessels").updateById(vessel_id, { programs: pgm }).write();
        },
        "add a program to a vessel, syntax: program [action] [program], e.g. program use say hi"
    ],
    "inspect": [
        (cmd) => {
            if (cmd.rest == "") {
                cmd.rest = vessel_location;
            }
            var targets = universe.locate(cmd.rest);
            if (targets.length > 1) {
                term.echo("more than one target vessel found:");
                targets.forEach((vessel) => {
                    term.echo("- " + get_vessel_name(vessel.id), { raw: true });
                })
                return true;
            };
            term.echo("Info for " + get_vessel_name(targets[0].id, true, true), { raw: true });
            term.echo(yaml.dump(targets[0]));
            return true;
        },
        "inspect a vessel"
    ],
    "spells": [
        (cmd) => {
            var spells_re = /.* spell$/;
            var spells = universe.search(spells_re);
            var spell_obj = {};
            spells.forEach((spell) => {
                var spell_name = get_vessel_name(spell.id, false, true);
                spell_obj[spell_name] = spell.programs;
            });
            term.echo("Found " + Object.keys(spell_obj).length + " spell(s):");
            term.echo(yaml.dump(spell_obj), { raw: false });
            return false;
        },
        "list all available spells"
    ],
    "cast": [
        (cmd) => {
            var parts = cmd.rest.split(" onto ");
            var spell_name = (parts[0] || "").trim() || undefined;
            var target_name = (parts[1] || "").trim() || undefined;
            var spells_re = /.* spell$/;
            var spells = universe.search(spells_re);
            var target;
            var found = [];
            spells.forEach((spell_) => {
                var spell_name_ = get_vessel_name(spell_.id, false, false);
                if (spell_name == spell_name_) {
                    found.push(spell_);
                }
            });
            if (found.length > 1) {
                term.error("found " + found.length + " matching spells");
                return;
            }
            if (found.length == 0) {
                term.error("no matching spell");
                return;
            };
            found = found[0]
            target = vessel;
            if (target_name) {
                target_name = target_name.trim();
                target = universe.find_visible(vessel_location, target_name);
                if (target.length > 1) {
                    term.error("found " + target.length + " target vessels");
                    return;
                }
                if (target.length == 0) {
                    term.error("no target found");
                    return;
                }
                target = target[0].id;
            }
            var old_vessel;
            if (vessel) {
                old_vessel = vessel.slice();
            }
            vessel = target;
            if (found.programs.cast) {
                run(found.programs.cast, term, false);
            } else {
                if (found.programs.use) {
                    run(found.programs.use, term, false);
                } else {
                    term.error("No matching program found")
                }
            }
            vessel = old_vessel;
            return false;
            //term.exec(cmd);
        },
        "Runs a spell program (optionally in the context of another vessel)"
    ]
}

Object.assign(cmds, vessel_cmds);

function push_send(data, type, to = undefined) {
    var chunk = { from: sw.id, to: to, type: type, data: data };
    console.log("SEND:", chunk);
    sw.publish(chunk);
}

function db_push(target) {
    var sb_snap = { vessels: {} };
    db.get("vessels").value().forEach((vessel) => {
        sb_snap.vessels[vessel.id] = new Date(vessel.t).toJSON();
    });
    console.log("pushing db", sb_snap, "to", target);
    push_send(sb_snap, 'state', target);
}

function handle_data(data) {
    console.log("RECV:", data);
    //term.echo(peer_id + ": " + yaml.dump(data))
    ev_channel.emit(data.type, data.data, data.from);
}

ev_channel.on('state', (data, from) => {
    var to_update = [];
    for (var vess_id in data.vessels) {
        var vess_t = new Date(data.vessels[vess_id]);
        var my_vessel = db.get("vessels").getById(vess_id).value();
        if (my_vessel == undefined) {
            to_update.push(vess_id);
        } else {
            var my_t = new Date(my_vessel.t)
            if (my_t < vess_t) {
                to_update.push(vess_id);
            }
        }
    }
    if (to_update.length > 0) {
        push_send(to_update, "get", from);
    };
});

ev_channel.on('get', (data, from) => {
    if (data.length != 0) {
        var ret = [];
        data.forEach((vess_id) => {
            ret.push(db.get("vessels").getById(vess_id).value());
        })
        push_send(ret, "update", from);
    };
});

function update_map() {
    var me = $('#map')[0];
    if (me.style.display == "block") {
        d3.selectAll("svg > *").remove();
        show_map();
    };
}

ev_channel.on('update', (data, from) => {
    data.forEach((vessel) => {
        db.get("vessels").upsert(vessel).write();
    });
    update_map();
});

function precmd(cmd) {
    if (vessel != undefined) {
        vessel_location = db.get("vessels").getById(vessel).value().parent;
    };
    try {
        cmd = eval_template(cmd);
    } catch (err) {
    }
    cmd = cmd.split(" && ");
    return cmd;
}

function scroll_to_bottom() {
    var cl = $(".cursor-line")[0];
    cl.scrollIntoView();
}

function postcmd() {
    var vessel_name = null;
    var location_name = null;
    var location_id;
    var note = null;
    location_id = vessel_location;
    vessel_name = "Ghost";
    if (vessel != undefined) {
        location_id = db.get("vessels").getById(vessel).value().parent;
        vessel_name = get_vessel_name(vessel);
    }
    if (location_id != undefined) {
        location_name = get_vessel_name(location_id);
        note = db.get("vessels").getById(location_id).value().note;
    } else {
        location_name = "Void";
    };
    term.echo("You are a " + vessel_name + " in the " + location_name, { raw: true });
    if (note != null) {
        term.echo(note, { raw: false });
    }
    var vis = universe.visible(location_id);
    if (vis.length > 0) {
        term.echo("You can see:");
        console.log("VIS", vis);
        vis.forEach((v) => {
            if (v.id == location_id) {
                return;
            }
            if (v.id == vessel) {
                return;
            }
            term.echo(" - " + get_vessel_name(v.id), { raw: true });
        })
    } else {
        term.echo("you can't see anything, why don't you <cmd>create</cmd> something", { raw: true });
    };
    return;
}


function run(command, term, force_post) {
    command = precmd(command);
    var do_post = false;
    command.forEach((cmd) => {
        if (!cmd) return;
        cmd = $.terminal.parse_command(cmd);
        var cmd_valid = false;
        console.log("CMD:", cmd);
        if (cmd.name in cmds) {
            cmd_valid = true;
            if (cmds[cmd.name][0](cmd) != true) {
                do_post = true;
            };
        } else {
            var vess = null;
            var loc = db.get("vessels").getById(vessel_location).value();
            if (vessel) {
                vess = db.get("vessels").getById(vessel).value();
                loc = db.get("vessels").getById(vess.parent).value();
            };
            do_post = true;
            universe.visible(loc.id).forEach((vis) => {
                if (vis.programs) {
                    Object.keys(vis.programs).forEach((pgm_cmd) => {
                        if (cmd.name == pgm_cmd) {
                            do_post = false;
                            var pgm = eval_template(vis.programs[pgm_cmd]);
                            term.exec(pgm);
                            cmd_valid = true;
                        };
                    })
                }
            });
        };
        if (!cmd_valid) {
            term.error("Invalid command: " + cmd.name);
            do_post = false;
        }
    })
    if (force_post != undefined) {
        do_post = force_post;
    }
    db_push();
    if (do_post) {
        postcmd();
    }
};

var opts = {
    greetings: false,
    exit: false,
    complete: false,
    convertLinks: true,
    historySize: false,
    describe: false,
    name: 'universe',
    prompt: (cb) => {
        var prompt_text;
        prompt_text = vessel_location;
        if (vessel != undefined) {
            vessel_location = db.get("vessels").getById(vessel).value().parent;
            prompt_text = vessel + '@' + vessel_location;
        }
        var prompt = '[[;#D72424;]' + prompt_text + ']';
        scroll_to_bottom();
        cb(new String('[' + prompt + ']> '));
    },
    login: false,
};

$('#terminal').terminal(run, opts);

var opts = {
    utp: true, tcp: false,
    gossip: {},
    port: conf.port,
};

if (conf.test != true) {
    opts.gossip.keys = universe_keys
}
var DEFAULT_OPTS = swarm_defaults(opts);
console.log(DEFAULT_OPTS);
var sw = pubsub(conf.instance, DEFAULT_OPTS);

sw.id = sw.gossip.keys.id;


sw.swarm.on("connecting", (peer) => {
    console.log(peer);
    //udp_punch(peer.host,peer.port)
})

sw.swarm.on("connection", (conn) => {
    console.log(conn);
    push_send({}, "hello");
    db_push();
})
sw.on('message', handle_data);


var term = $('#terminal').terminal();
window.term = term;
window.db = db;
window.sw = sw;
window.U = universe;
window.conf = conf;
window.$ = $;

var port = opts.port;

document.onclick = (ev) => {
    if (ev === undefined) { ev = window.event; }
    var target = 'target' in event ? event.target : event.srcElement;
    if (target.nodeName != undefined) {
        var node = target.nodeName;
        if (node.toLowerCase() == "vessel") {
            target = db.get("vessels").getById(target.id).value();
            if (vessel) {

            }
            //TODO: check if visible
            term.cmd().set("enter " + (target.attr.join(" ") + " " + target.name).trim());
            // else 
            //term.cmd().set("warp " + target.id);
        }
        if (node.toLowerCase() == "cmd") {
            term.cmd().set(target.textContent.trim());
        }
    }
};

asciify("Universe", 'doom', (err, res) => {
    if (err) {
        console.error(err);
        return;
    };
    opts.port = sw.swarm.address().port;
    term.echo(res.trimRight() + '\n');
    term.echo("Ready on port " + opts.port + " with ID " + sw.id);
    term.echo("Trying to forward port...");
    try {
        nat(opts.port, opts.port);
    } catch (err) {
        term.error("Automatic port-forwarding failed: " + err.msg);
    }
    if (conf.test == true) {
        term.echo("[[bg;#ff0000;]/!\\ Using non-persisten Database /!\\ ]")
    }
    postcmd();
    if (conf.cmd) {
        term.exec(conf.cmd);
    }
});
module.exports = $;
