# Universe
## A graph based multi-user interactive-fiction experiment

## Usage

### Building
`yarn dist`

### Executing
`yarn install && yarn electron .`

### Commands
- just run `help`

### Network Protocol

on startup the client run a pubsub-swarm listener which uses the bittorent DHT and MDNS for peer discovery
when a new peer is discovered and a UTP connection is established the following exchange takes place where N is the new peer and P is the peer already in the swarm
```
P -> N: {type:hello, data:{}}
P -> N: {type:state, data:<mapping of vessel_id->timestamp> }
```
at this point N compares the state-object it recieved to its local database and compiles a list of vessel ids that need updating (either because they are outdated or because they do not exist yet)
```
N -> P: {type:get, data: <list of vessel_ids>}
P -> N: {type:update, data: <list of vessel objects that N requested>}
```
after recieving the `update` message N updates its local database with the newly accquired information

this exchange is repeated after every command run by the user